<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <?php print $head ?>
    <title><?php print $head_title ?></title>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
  </head>
 <body>
 <?php print $admin ?>
<!-- header starts-->
<div id="header-wrap">
<div id="header" class="container_16">						
  <h1 id="logo-text"><a href="<?php print base_path() ?>" title="home"><?php print $site_name ?></a></h1>		
  <p id="intro"><?php print $site_slogan ?></p>				
		
<!-- navigation -->
<div  id="nav">
<?php print theme('links', $primary_links, array('class' => 'links primary-links')); ?>
</div>		
		
<div id="header-image"></div> 		
		
<div id="search"><?php print $search_box ?></div>
	
<!-- header ends here -->
</div>
</div>
	
<!-- content starts -->
<div id="content-outer">
<div id="content-wrapper" class="container_16">
	
<!-- main -->
<div class="tabs"><?php print $tabs ?></div>
<div id="main" class="grid_8">
	<?php print $content ?>
</div>
<!-- main ends -->
		
<!-- left-columns starts -->
<div id="left-columns" class="grid_8">
		
<div class="grid_4 alpha">
<div class="sidemenu">	
		<?php print $rightside ?>
</div>
		</div>
<div class="grid_4 omega">
		<?php print $featured ?>
</div>				
			
</div>	
		
<!-- end left-columns -->
</div>		
	</div>
<!-- contents end here -->	
</div>
</div>

	<!-- footer starts here -->	
	<div id="footer-wrapper" class="container_16">
	
		<div id="footer-content">
			<div class="grid_8">
				<?php print $footer_left ?>
			</div>	
						<div class="grid_8">
			<?php print $footer_right ?>
		</div>
		<div id="footer-bottom">
    <?php print $footer_message ?>	
		</div>	
	</div>
	<!-- footer ends here -->

</body>
<?php print $closure ?>
</html>
